$(document).ready(function(){
    var title = $('#title'),
        url = $('#url'),
        email = $('#email');

    function sendajax(data){
        $.ajax({
            url: '/suport',
            type: 'post',
            processData: false,
            contentType: false,
            // dataType: 'json',
            data: data,
            success:function(data) {
                $('.ok').html('Ваш запрос принят в обработку, ожидайте ответа');
                // $('#form-upload')[0].reset();
                $('#modal-suport').modal('hide');
                $('.ok').show();
                setTimeout(function(){$(".ok").fadeOut("slow")},4000);


            },
            error:function() {

            }
        });
    }
    function eror(text){
        $('#extn_error').show();
        $('#extn_error').html(text);
        // $(".stop").show();
        // setTimeout(function(){$(".stop").fadeOut("slow")},4000);

    }


    $('#img_path').change(function(){
        var filesExt = ['jpg', 'png', 'jpeg'];
        var parts = $('#img_path').val().split('.');
        if (filesExt.join().search(parts[parts.length - 1]) == -1)
        {
           var text = "выберите изображение в формате jpg, png, jpeg";
            $('#upload-btn').attr('disabled', 'disabled');
           return eror(text);
        }else{
            $('#extn_error').hide();
            $('#upload-btn').attr('disabled', false);
        }

    });


    $('#upload-btn').click(function() {

     var form = $('#form-upload');
        if(title.val() === ''){
            title.css('border-color', 'red');
            $('#titleHelp').html('<b style="color: red;">Заполните пожалуйста поле Заголовок</b>');
        }
        if(url.val() === '') {
            url.css('border-color', 'red');
            $('#urlHelp').html('<b style="color: red;">Заполните пожалуйста поле Url сайта</b>');
        }
        if(email.val() === ''){
            email.css('border-color', 'red');
            $('#emailHelp').html('<b style="color: red;">Заполните пожалуйста поле Email</b>');
        }
        if($('#terms').prop('checked') === false){
            $('#lable-terms').css('color', 'red');

        }
        if(title.val() !== '' && url.val() !== '' && email.val() !== '' && $('#terms').prop("checked") === true){
            var link = location.pathname;

            form.append('<input type="hidden" name="id1x" value="' + idOneX + '">');
            form.append('<input type="hidden" name="id1y" value="' + idOneY + '">');
            form.append('<input type="hidden" name="id2x" value="' + idTwoX + '">');
            form.append('<input type="hidden" name="id2y" value="' + idTwoY + '">');
            form.append('<input type="hidden" name="width" value="' + w + '">');
            form.append('<input type="hidden" name="height" value="' + h + '">');
            switch(link) {
                case'/':
                    form.append('<input type="hidden" name="page" value="account">');
                    break;
                case'/youtube':
                    form.append('<input type="hidden" name="page" value="youtube">');
                    break;
                case'/tekhnik':
                    form.append('<input type="hidden" name="page" value="tekhnik">');
                    break;
                case'/shop':
                    form.append('<input type="hidden" name="page" value="shop">');
                    break;
                case'/brand':
                    form.append('<input type="hidden" name="page" value="brand">');
                    break;
                case'/web':
                    form.append('<input type="hidden" name="page" value="web">');
                    break;
                case'/car':
                    form.append('<input type="hidden" name="page" value="car">');
                    break;
                case'/all':
                    form.append('<input type="hidden" name="page" value="all">');
                    break;
            }
            // console.log(form);
            var formData = new FormData(form.get(0));
            form.submit();
            // sendajax(formData);
        }
    });

    // ok.hide();
    // stop.hide();
//-----------------PROMOCOD--------------------------



    $('#promo-btn').click(function () {
        var promo = $('#promo').val();
        $.get('/activ-promo', {code: promo})
            .done(function(data){
                // console.log(data.split('-')[0]);
                var status = $('#promo-status');
                if(data.split('-')[0] === 'none'){
                    status.show().css('color', 'red').html('Промокод ненайден');
                }else if(data.split('-')[0] === 'active'){
                    status.show().css('color', 'red').html('Промокод уже был активирован ');
                }else{
                    status.show().css('color', 'green').html('Промокод на суму <span></span> активирован');
                    $('#promo-status > span').html(data.split('-')[0]+'грн');

                    suma = (Math.round(sum) * prc).toFixed(2);
                    $('.prc').html(Number(suma)-Number(data.split('-')[0]));
                    if(Number(suma) - Number(data.split('-')[0]) < 10){
                        $('.prc').html('10 грн');
                        $('#form-upload').append('<input type="hidden" name="promo" value="' + data.split('-')[1] + '">');
                    }else{
                        $('#form-upload').append('<input type="hidden" name="promo" value="' + data.split('-')[1] + '">');
                    }
                }
            });
    });


//------------------Suport----------------------------

    var suport = $('#form-suport');

    $('#suport-btn').click(function(){
        var title = $('#title-sup'),
            name = $('#name-sup'),
            email = $('#email-sup'),
            desc = $('#description-sup');
        if(title.val() === ''){
            title.css('border-color', 'red');
        }
        if(email.val() === ''){
            email.css('border-color', 'red');
        }
        if(desc.val() === ''){
            desc.css('border-color', 'red');
        }
        if(title.val() !== '' && email.val() !== '' && desc.val() !== ''){
            var suportData = new FormData(suport.get(0));

            sendajax(suportData);
        }

});
});