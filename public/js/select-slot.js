
$(document).ready(function(){
    var oneId = 0;
    var twoId = 0;
    var click = 0;

    var pixelh = 0;
    var pixelw = 0;
    var sum = 0;

    $('.block').on('click', function(){
       click++;

     if(click === 1) {
         oneId = $(this).attr('id');
         $(this).addClass('select-one');

         $('.block').on('mouseenter', function (e) {

             twoId = $(this).attr('id');

             var cordinatId1 = (oneId / 100).toFixed(2).split('.');
             var id1h = cordinatId1[0];
             var id1w = cordinatId1[1];
             var cordinatId2 = (twoId / 100).toFixed(2).split('.');
             var id2h = cordinatId2[0];
             var id2w = cordinatId2[1];
             var height = Number(id2h) - Number(id1h);
             var width = Number(id2w) - Number(id1w);
             var he = Number(oneId);

             $('.block').removeClass('select');

             for (var h = 0; h <= Math.abs(height); h++) {
                    var wid = he;
                    if (Number(id1h) > Number(id2h)) {
                        he = he - 100;
                        if (h === Math.abs(height)) {
                            he = he + 100;
                        }
                    } else {
                        he = he + 100;
                        if (h === Math.abs(height)) {
                            he = he - 100;
                        }
                    }

                    $('#' + he).attr('class', 'block select');

                    for (var w = 0; w < Math.abs(width); w++) {

                        if (Number(id1w) > Number(id2w)) {
                            wid = wid - 1;
                        } else {
                            wid = wid + 1;
                        }
                        // console.log(wid);
                        $('#' + wid).attr('class', 'block select');
                    }
             }

             sum = Math.abs((width + 1)) * Math.abs((height + 1));
             console.log(sum);
             pixelw = (width + 1) * 8;
             pixelh = (height + 1) * 5;
             $('#heig').html('В ' + Math.abs(pixelh) + 'px');
             $('#wid').html('Ш ' + Math.abs(pixelw) + 'px');
             $('#slot').html(sum);



         });
     }
     if(click === 2){

         // console.log(2);
         $('.block').off('mouseenter');
         $(this).addClass('select-two');

     }else if(click === 3){

         // console.log(3);
         click = 0;
         $('#'+oneId).removeClass('select-one');
         $('#'+twoId).removeClass('select-two');
         $('.block').removeClass('select');
         pixelh = 0;
         pixelw = 0;
         sum = 0;
         $('#heig').html('В ' + Math.abs(pixelh) + 'px');
         $('#wid').html('Ш ' + Math.abs(pixelw) + 'px');
         $('#slot').html(sum);
     }
    });

});
