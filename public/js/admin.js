$(document).ready(function () {
    $('.punct-l').click(function(){
       var link =  $(this).find('a').attr('href');
        $(location).attr('href', link);
    });
    $('#nameCompany').focusout(function(e){
        var value = $(this).val();
        $('.name-company').html(value);

    });
    $('#aProm').change(function(){
        var value = $(this).val(),
            text = $('#aProm option:selected').text();
        text = text.split(',');
        text = text[1];
        $('.promo-company').html(value);
        $('.sum').html(text);
        // console.log('sda');
    });
    $('#create-mail-form').submit(function(e){
        e.preventDefault();
        var forma = $('#create-mail-form');
        var form = new FormData(forma.get(0));
    //     $.post('/admin/send-mail', {form: form})
    //         .done(function(data){
    //             console.log(data);
    //         });
        $.ajax({
            url: '/admin/send-mail',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            success: function(data){
                console.log(data);
                    forma[0].reset();
                    $('.ok').html('Email відправлено!');
                    $('.ok').show();
                    setTimeout(function(){$(".ok").fadeOut("slow")},4000);

            },
            error:function() {

            }


        });
    });
    $('#create-promo').submit(function(e){
        e.preventDefault();
        var formPromo = $('#create-promo');
        var form  = new FormData(formPromo.get(0));
        $.ajax({
            url: '/admin/create-promo',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            success: function(data){
                console.log(data);
                formPromo[0].reset();
                $('.ok').html('Промокод додано!');
                $('.ok').show();
                setTimeout(function(){$(".ok").fadeOut("slow")},4000);
            },
            error: function(){
                $('.stop').html('Щось не так!');
                $('.stop').show();
                setTimeout(function(){$(".stop").fadeOut("slow")},4000);
            }
        });
    })
});