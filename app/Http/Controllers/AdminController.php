<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promo;
use App\SendMail;
use Mail;

class AdminController extends Controller
{
    public function doWelcom()
    {
        $data = [
            'title' => 'Admin',
//            'oplink' => '/login'
            'welcom' => 'punct-active'
        ];
        return view('admin.content', $data);

    }
    public function doMailCreate()
    {
        $data = [
            'email' => 'punct-active',
        ];
        return view('admin.element.mail', $data);
    }
    public function doPromo()
    {
        $data = [
            'title' => 'world-top Промокоди',
            'promo' => 'punct-active',
        ];
        return view('admin.element.promo', $data);
    }
    public function doSendMail(Request $request)
    {
        $email = $request->email;
        Promo::where('code',$request->promo)->update(['send' => 1]);

        $suma = Promo::where('code',$request->promo)->pluck('sum');

        $mailDB = new SendMail();

        $mailDB->name = $request->name;
        $mailDB->team = $request->team;
        $mailDB->section = $request->section;
        $mailDB->promo_code = $request->promo;
        $mailDB->email = $email;
        $mailDB->tamplete = 1;
        $mailDB->save();
        if($request->langue === 'ua'){
            $mail = 'email/mail';
        }else if($request->langue === 'ru'){
            $mail = 'email/mail-ru';
        }
        $data = [
            'name' => $request->name,
            'code' => $request->promo,
            'sum' => $suma[0],
            'link' => $request->section,
        ];
        Mail::send($mail, $data, function($message) use($email){
            $message->from('world.top.site@gmail.com', 'World-top');
            $message->to($email)->subject('World-top');
        });

        $status = "ok";
        return $status;
    }
    public function doCreatePromo(Request $request)
    {
        $promo = new Promo();
        $promo->code = $request->code;
        $promo->sum = $request->sum;
        $promo->active = 1;
        $promo->save();

        $status = "ok";
        return $status;
    }
}
