<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Youtube;
use App\Tekhnik;
use App\Shop;
use App\Brand;
use App\Web;
use App\Car;
use App\All;

class PaymentController extends Controller
{
//   public function doFormPayment(Request $request)
//   {
//        return view('element.payment-form', $request);
//   }
   public function doSucess(Request $request)
   {


       $status = $request->ik_inv_st;
       if($status === 'success'){
           $string = $request->ik_pm_no;
           $str = explode('_', $string);
           $id = $str[1];
           $page = $str[2];
           switch($page){
               case "account":
//                   $account = Account::find($id);
//                   $account->active = 1;
//                   $account->save();
                   return redirect('/');
               case "youtube":
//                   $account = Youtube::find($id);
//                   $account->active = 1;
//                   $account->save();
                   return redirect('/youtube');
               case "tekhnik":
//                   $account = Tekhnik::find($id);
//                   $account->active = 1;
//                   $account->save();
                   return redirect('/tekhnik');
               case "shop":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
                   return redirect('/shop');
               case "brand":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
                   return redirect('/barnd');
               case "web":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
                   return redirect('/web');
               case "car":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
                   return redirect('/car');
               case "all":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
                   return redirect('/all');
           }
       }
       return 'ok';
   }
   public function doError(Request $request)
   {
       $string = $request->ik_pm_no;
       $str = explode('_', $string);
       $id = $str[1];
       $page = $str[2];
       switch($page){
           case "account":
//                   $account = Account::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/');
           case "youtube":
//                   $account = Youtube::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/youtube');
           case "tekhnik":
//                   $account = Tekhnik::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/tekhnik');
           case "shop":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/shop');
           case "brand":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/barnd');
           case "web":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/web');
           case "car":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/car');
           case "all":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/all');
       }
   }
   public function doResult(Request $request)
   {
        $dataSet = $_POST;
        unset($dataSet['ik_sign']); //видаляємо з даних строку підпису
        ksort($dataSet, SORT_STRING); // сортуємо по ключам в алфавітному порядку елементи масиву
        array_push($dataSet, 'v3cwkJQzzhKGeVKr'); // додаємо в кінець масиву "секретний ключ"
        $signString = implode(':', $dataSet); // конкатенуємо значення через символ ":"
        $sign = base64_encode(md5($signString, true)); // беремо MD5 хеш в бінарному вигляді по
       if ($sign != $_POST['ik_sign']){
           exit('ошибка обработки платежа');
       }else {


           $string = $request->ik_pm_no;
           $str = explode('_', $string);
           $id = $str[1];
           $page = $str[2];
           switch ($page) {
               case "account":
                   $account = Account::find($id);
                   $account->active = 1;
                   $account->save();
                   break;
//                  return redirect('/');
               case "youtube":
                   $account = Youtube::find($id);
                   $account->active = 1;
                   $account->save();
                   break;
//                   return redirect('/youtube');
               case "tekhnik":
                   $account = Tekhnik::find($id);
                   $account->active = 1;
                   $account->save();
                   break;
//                   return redirect('/tekhnik');
               case "shop":
                   $account = Shop::find($id);
                   $account->active = 1;
                   $account->save();
                   break;
               case "brand":
                   $account = Brand::find($id);
                   $account->active = 1;
                   $account->save();
                   break;
               case "web":
                   $account = Web::find($id);
                   $account->active = 1;
                   $account->save();
                   break;
               case "car":
                   $account = Car::find($id);
                   $account->active = 1;
                   $account->save();
                   break;
               case "all":
                   $account = All::find($id);
                   $account->active = 1;
                   $account->save();
                   break;
//                   return redirect('/shop');
           }
       }

   }
   public function doExpectation(Request $request)
   {
       $string = $request->ik_pm_no;
       $str = explode('_', $string);
       $id = $str[1];
       $page = $str[2];
       switch($page){
           case "account":
//                   $account = Account::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/');
           case "youtube":
//                   $account = Youtube::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/youtube');
           case "tekhnik":
//                   $account = Tekhnik::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/tekhnik');
           case "shop":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/shop');
           case "brand":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/barnd');
           case "web":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/web');
           case "car":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/car');
           case "all":
//                   $account = Shop::find($id);
//                   $account->active = 1;
//                   $account->save();
               return redirect('/all');
       }
   }
   public function doAdvStatus(Request $request)
   {

//       $key = hash('sha256',$request->ac_transfer.':'.
//       $request->ac_start_date.':'.
//       $request->ac_sci_name.':'.
//       $request->ac_src_wallet.':'.
//       $request->ac_dest_wallet.':'.
//       $request->ac_order_id.':'.
//       $request->ac_amount.':'.
//       $request->ac_merchant_currency.':'.
//       '81$Yx8VC5i');
//       $str = $request->ac_order_id;
//
//       if($key === $request->ac_hash){
//        $string = explode('_', $str);
//        $id = $string[1];
//        $page = $string[2];
//        switch($page){
//               case "account":
//                   $account = Account::find($id);
//                   $account->active = 1;
//                   break;
//               case "youtube":
//                   $account = Youtube::find($id);
//                   $account->active = 1;
//                   break;
//               case "tekhnik":
//                   $account = Tekhnik::find($id);
//                   $account->active = 1;
//                   break;
//
//           }
//       }else{
//           return 'error';
//       }


   }
   public function dotest(Request $request)
   {
   }
}
