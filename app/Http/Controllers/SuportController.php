<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suport;
use Mail;

class SuportController extends Controller
{
    public function doSuport(Request $request)
    {
        $suport = new Suport();

        $suport->name = $request->name;
        $suport->title = $request->title;
        $suport->email = $request->email;
        $suport->description = $request->description;
        $suport->save();
        $dataEmail = [
            'name' => $request->name,
            'title' => $request->title,
            'email' => $request->email,
            'description' => $request->description,
        ];

        Mail::send('email/suport', $dataEmail, function($message)
        {
            $message->from('world.top.site@gmail.com', 'World-top');
            $message->to('world.top.site@gmail.com')->subject('suport world top');
        });

        return "ok";
    }
}
