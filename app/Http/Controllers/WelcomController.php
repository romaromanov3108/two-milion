<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation;
use Intervention\Image\ImageManagerStatic as Image;
use App\Account;
use App\Youtube;
use App\Promo;
use App\Shop;
use App\Brand;
use App\Web;
use App\Car;
use App\All;
use Storage;
use App\Tekhnik;

class WelcomController extends Controller
{
    public function doTest($lan)
    {
        if($lan === 'ua') {
            return view('test-email');
        }else if($lan === 'ru'){
            return view('test-email-ru');
        }

    }
    public function doWelcom()
    {
        $image = Account::where('active', 1)->get();
        $title = "World-top";
        $data = [
            'image' => $image,
            'title' => $title,
            'oplink' => '',
        ];
        return view('element.content_v2', $data);
    }
    public function doYoutub()
    {
        $image = Youtube::where('active', 1)->get();
        $title = "World-top YouTube";
        $data = [
            'image' => $image,
            'title' => $title,
            'oplink' => 'youtube',
        ];
        return view('element.content_v2', $data);
    }
    public function doShop()
    {
        $image = Shop::where('active', 1)->get();
        $title = "World-top Shop";
        $data = [
            'image' => $image,
            'title' => $title,
            'oplink' => 'shop'
        ];
        return view('element.content_v2', $data);
    }
    public function doTekhnik()
    {
        $image = Tekhnik::where('active', 1)->get();
        $title = "World-top technology brands";
        $data = [
            'image' => $image,
            'title' => $title,
            'oplink' => 'tekhnik',
        ];
        return view('element.content_v2', $data);
    }
    public function doBrand()
    {
        $image = Brand::where('active', 1)->get();
        $title = "World-top Brands";
        $data = [
            'image' => $image,
            'title' => $title,
            'oplink' => 'brand',
        ];
        return view('element.content_v2', $data);
    }
    public function doWeb()
    {
        $image = Web::where('active', 1)->get();
        $title = "World-top Web";
        $data = [
            'image' => $image,
            'title' => $title,
            'oplink' => 'web',
        ];
        return view('element.content_v2', $data);
    }
    public function doCar()
    {
        $image = Car::where('active', 1)->get();
        $title = "World-top Cars";
        $data = [
            'image' => $image,
            'title' => $title,
            'oplink' => 'car',
        ];
        return view('element.content_v2', $data);
    }
    public function doAll()
    {
        $image = All::where('active', 1)->get();
        $title = "World-top All";
        $data = [
            'image' => $image,
            'title' => $title,
            'oplink' => 'all',
        ];
        return view('element.content_v2', $data);
    }


    public function doUpload(Request $request)
    {
//       $this->validate($request, [
//            'title' => 'required|max:255',
//            'email' => 'required|email',
//            'url' => 'required',
//            'img' => 'img'
//
//        ]);
//-----------IMAGE-----------------------------------------------------
//        dd($request);
        switch ($request->page) {
            case "account":
                $id = Account::max('id') + 1;
                $account = new Account();
                $page = 'account';
                break;
            case "youtube":
                $id = Youtube::max('id') + 1;
                $account = new Youtube();
                $page = 'youtube';
                break;
            case "tekhnik":
                $id = Tekhnik::max('id') + 1;
                $account = new Tekhnik();
                $page = 'tekhnik';
                break;
            case "shop":
                $id = Shop::max('id') + 1;
                $account = new Shop();
                $page = 'shop';
                break;
            case "brand":
                $id = Brand::max('id') + 1;
                $account = new Brand();
                $page = 'brand';
                break;
            case "web":
                $id = Web::max('id') + 1;
                $account = new Web();
                $page = 'web';
                break;
            case "car":
                $id = Car::max('id') + 1;
                $account = new Car();
                $page = 'car';
                break;
            case "all":
                $id = All::max('id') + 1;
                $account = new All();
                $page = 'all';
                break;
        }
        $img = $request->file('img');
        $ext = $request->file('img')->getClientOriginalExtension();
        $nameTmp = $request->file('img')->getBasename();
        $name = explode('.', $nameTmp);
        $name = $name[0].'.'.$ext;
        $width = round($request->width);
        $height = round($request->height);
        $imgSrc = public_path("img/upload/".$id."/");

        Storage::disk('upload')->makeDirectory('upload/'.$id);

        Image::make($img)->resize($width, $height)->save($imgSrc.$name);
//--------PLANE--------------------------
        $plane = '';
        $id1x = round($request->id1x);
        $id1y = round($request->id1y);
        $id2x = round($request->id2x);
        $id2y = round($request->id2y);

        if($id1x < $id2x){
            if($id1y < $id2y){
                $plane = 'c';
            }else{
                $plane = 'b';
            }
        }else{
            if($id1y < $id2y){
                $plane = 'd';
            }else{
                $plane = 'a';
            }
        }
//------------/PLANE----------------

//-----------DB--------------------------
        $prc = 0.20;
        $sum = $request->width * $request->height;
        $sum = round($sum) * $prc;
        $sum = round($sum, 2);

        if($request->promo !== null){
           $promo = Promo::find($request->promo);

           $sum = $sum - $promo->sum;
           if($sum < 10){
               $sum = 10;
           }
        }


//        dd($sum);
//
        $account->title = $request->title;
        $account->email = $request->email;
        $account->url = $request->url;
        $account->description = $request->description;
        $account->img_path = "/img/upload/".$id."/".$name;
        $account->plane = $plane;
        $account->id1x = $id1x;
        $account->id1y = $id1y;
        $account->id2x = $id2x;
        $account->id2y = $id2y;
        $account->width = $width;
        $account->height = $height;
        $account->sum = $sum;
        $account->active = 0;
        $account->save();
        $description = 'оплата за пиксели '.$width*$height;

        $data = [
            'id' => $id,
            'time' => time(),
            'sum' => $sum,
            'cur' => 'UAH',
            'desc' => $description,
            'page' => $page,
        ];
//        return view('element.adv-form', $data);
        return view('element.payment-form', $data);

/// //-----------/DB--------------------------
    }
    public function doPromo(Request $request)
    {
        $promos = Promo::where('code', $request->code)->get();

            if(count($promos) === 0){
                $status = 'none-0';
            }else{
                foreach($promos as $promo) {
                }
                    if($promo->active === 0){
                        $status = 'active-0';
                    }else{
                        $status = $promo->sum.'-'.$promo->id;
                        $dis = Promo::find($promo->id);
                        $dis->active = 0;
                        $dis->save();
                    }

            }

        return $status;
    }

}
