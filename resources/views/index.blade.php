<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>{{$title or "World-top"}}</title>
    <meta name="description" content="world-top сайт на котором можно разместить логотип своей компании, описание. и ссылки на Компнию спомощью этого вы сможете доказать статус своей компании своим клиентам и увеличить количество клиентов своей компании.">
    <meta name="keywords" content="world-top мировой рейтинг топ компании рейтинг компании">
    <meta property="og:title" content="{{$title or 'world-top'}}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://world-top.site/{{$oplink or ''}}" />
    <meta property="og:image" content="http://world-top.site/img/logo.png" />
    <meta name="interkassa-verification" content="e0870130e494928669d177d131d9da91" />
    <meta name="google-site-verification" content="g8Fr3kDvvwnuTcvnKEw9zwsp3gGRlJ-xr1H0MbikLos" />
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <noscript>ВКЛЮЧИТЕ JAVASCRIPT БЕЗ нево ничего не работает</noscript>
</head>
<body>

@yield('error')

@yield('content')
<footer>
    <span style="float: left;">world.top.site@gmail.com</span>
    <span>&#169;World-site</span>

    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-suport" style="float: right;">Связаться с нами</button>
</footer>
@include('element.modal.explanation')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="/js/generate_v2.js"></script>
<script src="/js/ajax.js"></script>
<script src="/js/popup.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117759126-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-117759126-1');
</script>

</body>
</html>