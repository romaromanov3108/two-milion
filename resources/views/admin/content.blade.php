@extends('index')
@section('content')
    @include('admin.element.header')
    <div class="row">
    @include('admin.element.left-menu')
        <div class="col-9">
    @yield('adminContent')
        </div>
    @include('element.alert')
    </div>
    <script src="/js/admin.js"></script>
@endsection