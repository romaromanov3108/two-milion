

        <div class="row">
      <div class="col-5">
        <h3>Створити Розсилку</h3>
        <form method="post" action="{{route('send-mail')}}" id="create-mail-form">
            @csrf
            <div class="form-group">
                <label for="team">Тема</label>
                <input type="text" class="form-control form-control-sm" name="team" id="team" placeholder="World-top" required>
            </div>
            <div class="form-group">
                <label for="nameCompany">Имя компании</label>
                <input type="text" class="form-control form-control-sm" name="name" id="nameCompany" placeholder="World-top" required>
            </div>
            <div class="form-group">
                <label for="apromo">Промо код</label>

                    <select class="form-control form-control-sm" name="promo" id="aProm">
                        <option value="0">Нет</option>
                        @foreach(\App\Promo::where('active', 1)->where('send', 0)->get() as $promo)
                        <option value="{{$promo->code}}">{{$promo->code}}, {{$promo->sum}}грн</option>
                        @endforeach
                    </select>
            </div>
            <div class="form-group">
                <label for="section-email">Розділ</label>

                <select class="form-control form-control-sm" name="section" id="section-email">
                    <option value="youtube">Youtube</option>
                    <option value="tekhnik">Техника</option>
                    <option value="shop">Магазини</option>
                    <option value="brand">Бренди</option>
                    <option value="web">WEB</option>
                    <option value="all">ALL</option>
                    <option value="car">Машини</option>
                </select>
            </div>
            <div class="form-group">
                <label for="langue-email">Мова</label>

                <select class="form-control form-control-sm" name="langue" id="langue-email">
                    <option value="ua">Українська</option>
                    <option value="ru">Руский</option>
                </select>
            </div>
            <div class="form-group">
                <label for="emailSend">Емайл куда відправляти</label>
                <input type="text" class="form-control form-control-sm" name="email" id="emailSsend" placeholder="example@gmail.com" required>

            </div>
            <input type="submit" class="btn btn-success" value="Отправить">
        </form>
      </div>



       <div class="col-7">
          <table class="body-wrap">
              <tr>
                  <td class="container">

                      <!-- Message start -->
                      <table>
                          <tr>
                              <td align="center" class="masthead">

                                  <!-- <h1>world-top</h1> -->
                                  <img src="http://world-top.site/img/logo.png" style="height: 75%;">

                              </td>
                          </tr>
                          <tr>
                              <td class="content">

                                  <h2>
                                      Доброго дня, адміністрація компанії <span class="name-company"></span></h2>

                                  <p>Пропонуємо вам
                                      розмістити логотип <span class="name-company"></span>, це затвердить статус компанії <span class="name-company"></span>  в очах своїх клієнтів
                                      та показати маштабність<span class="name-company"></span>, відносно своїх конкурентів.
                                      Завдяки розміщенню логотипу <span class="name-company"></span> на сайті <a href="http://world-top.site/">world-top</a>.<br>
                                      В честь старту <a href="http://world-top.site/">world-top</a> акційна ціна за 1 піксель складає 0.20грн.
                                      <br><span style="color:red;">* З 6 травня ціна буде в два рази більшою.</span><br>
                                      Також ми вирішили надати вам персональний промокод: <b class="promo-company"></b> на суму <b class="sum"></b>.<br>
                                      <span style="color:red;">* Увага промокод може бути активований тільки один раз, та дійсний протягом 3 днів з моменту відправлення цієї пропозиції.</span></p>

                                  <table>
                                      <tr>
                                          <td align="center">
                                              <p>
                                                  <a href="http://world-top.site/web" target="_blank" class="buttonex">Вибрати місце розміщення</a>
                                              </p>
                                          </td>
                                      </tr>
                                  </table>

                                  <p>
                                      Викуплені вами пікселі можна за бажанням перепродати <b>за власною ціною</b>.</p>

                                  <p><em>–З повагою адміністрація World-top.</em></p>

                              </td>
                          </tr>
                      </table>

                  </td>
              </tr>
              <tr>
                  <td class="container">

                      <!-- Message start -->
                      <table>
                          <tr>
                              <td class="content footer" align="center">
                                  <p>Відправлено від <a href="http://world-top.site/">World-top</a></p>
                                  <p><a href="mailto:world.top.site@gmail.com">world.top.site@gmail.com</a> | <a href="http://world-top.site/">World-top</a></p>
                              </td>
                          </tr>
                      </table>

                  </td>
              </tr>
          </table>
      </div>
    </div>


