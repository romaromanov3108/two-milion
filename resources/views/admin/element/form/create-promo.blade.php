
    <h5 style="text-align: center;">Добавить промокод</h5>
    <form class="form-inline" id="create-promo">
        @csrf
        <div class="form-group mb-2" >
            <input type="text" name="code" class="form-control form-control-sm" id="code-add" placeholder="code" required>
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <input type="text" name="sum" class="form-control form-control-sm" id="sum-add" placeholder="сума" required>
        </div>
        <button type="submit" class="btn btn-primary mb-2 btn-sm">Добавить Промокод</button>
    </form>

