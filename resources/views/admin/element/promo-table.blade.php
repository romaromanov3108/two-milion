<h5 style="text-align: center;">Список Промокодов</h5>

<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Промо-код</th>
        <th scope="col">Сума</th>
        <th scope="col">Активний</th>
        <th scope="col">Відправлений</th>
        <th scope="col">стоврений</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach(\App\Promo::all() as $promo)
    <tr>
        <th scope="row">{{$promo->id}}</th>
        <th>{{$promo->code}}</th>
        <td>{{$promo->sum}}</td>
        <td>{{$promo->active}}</td>
        <td>{{$promo->send}}</td>
        <td>{{$promo->created_at}}</td>
    </tr>
    @endforeach

    </tbody>
</table>