<nav class="navbar navbar-dark sticky-top menu">
    <h3>World-top</h3>
    {{--<div class="col-lg-2">--}}
    {{--<select class="form-control form-control-sm select-menu">--}}
    {{--<option><a href="/web">Web</a></option>--}}
    {{--<option>sdsss</option>--}}
    {{--</select>--}}
    {{--</div>--}}
    <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
        {{--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
            {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
        {{--</a>--}}


            <a class="btn btn-danger" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

    </li>
    </ul>
    <script src="{{ asset('/js/app.js') }}"></script>
</nav>