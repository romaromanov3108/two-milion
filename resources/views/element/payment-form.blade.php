<form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
    <input type="hidden" name="ik_co_id" value="5ac33d163d1eaff0668b4574">
    <input type="hidden" name="ik_pm_no" value="ID_{{$id}}_{{$page}}_{{$time}}">
    <input type="hidden" name="ik_am" value="{{$sum}}">
    <input type="hidden" name="ik_cur" value="{{$cur}}">
    <input type="hidden" name="ik_desc" value="{{$desc}}">
</form>
<script>
    document.getElementById("payment").submit();
</script>