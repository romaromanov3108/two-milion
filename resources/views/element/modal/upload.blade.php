<div class="modal fade" id="modal-upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Заполните необходимые поля</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="upload-form">
               <form id="form-upload" enctype="multipart/form-data" action="/upload" method="post">
                   @csrf
                   <div class="form-group">
                       <label for="img">выберите изображение</label>

                       <input type="file" name="img" id="img_path" onchange='openFile(event)' aria-describedby="imgHelp">

                       <small id="imgHelp" class="form-text text-muted">
                           <span style="color: red;"> * размер изображения будет сжат до <b class="heig">В 0px</b>, <b class="wid">Ш 0px</b> </span>
                       </small>

                       <div class="img_prev">
                           <img id='output'>
                       </div><br>
                       <span id="extn_error" style="color: red; font-size: 0.9em;"></span>

                   </div>
                   <div class="form-group">
                       <label for="title">Заголовок</label>
                       <input type="text" class="form-control" name="title" aria-describedby="titleHelp" id="title" required placeholder="Заголово">
                       <small id="titleHelp" class="form-text text-muted">Заголовок или название изображения</small>
                   </div>
                   <div class="form-group">
                       <label for="email">Email</label>
                       <input type="text" class="form-control" name="email" aria-describedby="emailHelp" id="email" required placeholder="example@gmail.com">
                       <small id="emailHelp" class="form-text text-muted">Ваша электронная почта</small>
                   </div>
                   <div class="form-group">
                       <label for="url">Url</label>
                       <input type="text" class="form-control" name="url" aria-describedby="urlHelp" id="url" required placeholder="http://www.site.com">
                       <small id="urlHelp" class="form-text text-muted">Ссылка на сайт или изображения</small>
                   </div>
                   <div class="form-group">
                       <label for="description">Описание</label>
                       <textarea class="form-control" name="description" aria-describedby="descriptionHelp" style="height: 200px;" id="description" placeholder="Описание"></textarea>
                       <small id="descriptionHelp" class="form-text text-muted">Описание изображению или сайтау</small>
                   </div>
               </form>
               <div class="form-inline">
                  <div class="form-group mb-2">
                    <label for="promo">Промокод:</label>
                  </div>
                  <div class="form-group mb-2 mx-3">
                    <input type="text" class="form-control" name="promo" aria-describedby="promoHelp" id="promo">
                  </div>
                  <button class="btn btn-primary btn-sm mb-2" id="promo-btn">Активировать</button>
               </div>
               <b id="promo-status" style="color: green; display: none;">Промокод на суму <span></span> активирован</b>
               <small id="promoHelp" class="form-text text-muted">
                 Введите промокод и нажмите активировать<br>
                 <b style="color: red;">Внимание промокод можно активировать только один раз, после нажатия кнопки активировать повторное использование промокод будет невозможно.</b></small>
              </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="terms">
                    <label class="form-check-label" for="terms" id="lable-terms">Я прочитал и принимаю условия <a href="#" id="a-terms">Пользовательское соглашение</a></label>
                </div>



                <button type="button" class="btn btn-success" id="upload-btn" disabled>Сохранить и оплатить <b class="prc">0 грн</b></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
               </div>
            </div>
        </div>
    </div>
</div>
