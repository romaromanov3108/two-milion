<div class="modal fade" id="modal-suport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Связаться с нами</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="upload-form">
                    <form id="form-suport" enctype="multipart/form-data" action="/suport" method="post">
                        @csrf
                        {{--<div class="form-group">--}}
                            {{--<label for="img">изображение</label>--}}
                            {{--<input type="file" name="img" id="img_path" aria-describedby="imgHelp">--}}

                        {{--</div>--}}
                        <div class="form-group">
                            <label for="title">Тема</label>
                            <input type="text" class="form-control" name="title" aria-describedby="titleHelp" id="title-sup" required placeholder="Оплата">
                            <small id="titleHelp" class="form-text text-muted">Тема обращения</small>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" aria-describedby="emailHelp" id="email-sup" required placeholder="example@gmail.com">
                            <small id="emailHelp" class="form-text text-muted">ваша электронная почта</small>
                        </div>
                        <div class="form-group">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" name="name" aria-describedby="nameHelp" id="name-sup" placeholder="Петро">
                            <small id="urlHelp" class="form-text text-muted">как к вам обращаться</small>
                        </div>
                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea class="form-control" name="description" required aria-describedby="descriptionHelp" style="height: 200px;" id="description-sup" placeholder="Описание проблемы"></textarea>
                            <small id="descriptionHelp" class="form-text text-muted">Описание проблемы</small>
                        </div>


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="suport-btn">отправить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
