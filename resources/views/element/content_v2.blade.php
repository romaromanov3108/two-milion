@extends('index')
@section('content')
@include('element.menu')
<div class="block-v2">
    <div class="select-block"></div>
   @foreach($image as $img)
    <div class="img" id="{{$img->id}}"
         @if($img->plane === 'a')
         style="top: {{$img->id2y}}; left: {{$img->id2x}}">
         @elseif($img->plane === 'b')
         style="top: {{$img->id2y}}; left: {{$img->id1x}}">
         @elseif($img->plane === 'c')
         style="top: {{$img->id1y}}; left: {{$img->id1x}}">
         @elseif($img->plane === 'd')
         style="top: {{$img->id1y}}; left: {{$img->id2x}}">
         @endif
        {{--{{$img->title}}--}}
        <img src="{{ $img->img_path }}">
    </div>
        <div class="block-popup" id="block-{{$img->id}}">
            <h6>{{$img->title}}</h6>
            <img src="{{$img->img_path}}">
            <span>Веб сайт:</span>
            <a href="{{$img->url}}" target="_blank">{{$img->url}}</a>
            <p>{{$img->description}}</p>
        </div>
    @endforeach

</div>
@include('element.modal.upload')
@include('element.modal.suport')
@include('element.modal.terms')
@include('element.alert')

@endsection