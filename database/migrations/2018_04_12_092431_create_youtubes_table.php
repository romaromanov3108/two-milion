<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoutubesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtubes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 55);
            $table->string('url')->nullable();
            $table->string('email');
            $table->text('description')->nullable();
            $table->string('img_path');
            $table->string('plane');
            $table->integer('id1x');
            $table->integer('id1y');
            $table->integer('id2x');
            $table->integer('id2y');
            $table->integer('width');
            $table->integer('height');
            $table->float('sum');
            $table->boolean('active');
            $table->boolean('divider')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtubes');
    }
}
