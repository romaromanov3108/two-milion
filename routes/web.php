<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomController@doWelcom');
Route::get('/youtube', 'WelcomController@doYoutub');
Route::get('/tekhnik', 'WelcomController@doTekhnik');
Route::get('/shop', 'WelcomController@doShop');
Route::get('/brand', 'WelcomController@doBrand');
Route::get('/web', 'WelcomController@doWeb');
Route::get('/all', 'WelcomController@doAll');
Route::get('/car', 'WelcomController@doCar');
Route::get('/test', 'PaymentController@dotest');
Route::get('/test-mail/{lan}', 'WelcomController@doTest');
Route::get('/activ-promo', 'WelcomController@doPromo');

Route::post('/upload', 'WelcomController@doUpload');
Route::post('/payment-sucess', 'PaymentController@doSucess');
Route::post('/payment-error', 'PaymentController@doError');
Route::post('/payment-result', 'PaymentController@doResult');
Route::post('/payment-expectation', 'PaymentController@doExpectation');
Route::post('/adv-status', 'PaymentController@doAdvStatus');
Route::post('/suport', 'SuportController@doSuport');

Route::group(['middleware' => 'auth'], function(){
    Route::group(['prefix' => 'admin'], function(){
        Route::get('create-email', 'AdminController@doMailCreate')->name('create-email');
        Route::get('welcom', 'AdminController@doWelcom')->name('welcom');
        Route::get('promocode', 'AdminController@doPromo')->name('promo-code');

        Route::post('send-mail', 'AdminController@doSendMail')->name('send-mail');
        Route::post('create-promo', 'AdminController@doCreatePromo')->name('create-promo');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
